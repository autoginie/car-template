'use strict';

/**
 * @ngdoc function
 * @name webportalApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the webportalApp
 */
angular.module('webportalApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

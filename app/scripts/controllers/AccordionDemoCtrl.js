'use strict';

angular.module('webportalApp')
    .controller('AccordionDemoCtrl', function () {
        this.groups = [
        {
          title: 'Dynamic Group Header - 1',
          content: 'Dynamic Group Body - 1'
        },
        {
          title: 'Dynamic Group Header - 2',
          content: 'Dynamic Group Body - 2'
        }
      ];
    })
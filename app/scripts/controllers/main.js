'use strict';

/**
 * @ngdoc function
 * @name webportalApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webportalApp
 */
angular.module('webportalApp')
  .controller('MainCtrl', function () {
    this.service = false;
    this.repairs = false;
    this.denting = false;

    this.showService = function () {
        this.service = true;
        this.repairs = false;
        this.denting = false;
    };

    this.showRepairs = function () {
        this.service = false;
        this.repairs = true;
        this.denting = false;
    };

    this.showDenting = function () {
        this.service = false;
        this.repairs = false;
        this.denting = true;
    };
    this.oneAtATime = true;
    // this.status = {
    //     isCustomHeaderOpen: false,
    //     isFirstOpen: true,
    //     isFirstDisabled: false
    //   };
  });

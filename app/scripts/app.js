'use strict';

/**
 * @ngdoc overview
 * @name webportalApp
 * @description
 * # webportalApp
 *
 * Main module of the application.
 */
angular
  .module('webportalApp', [
    'ngAnimate',
    'ngRoute',
    'ngTouch',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
